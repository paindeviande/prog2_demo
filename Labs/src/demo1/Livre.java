package demo1;

public class Livre extends TaxeSimple {

	public Livre(float prix) {
		super(prix);
	}
	
	@Override
	public String type()
	{
		return "livre";
	}
}
