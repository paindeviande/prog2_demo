package demo1;

public class Meuble extends TaxeDouble {

	public Meuble(float prix) {
		super(prix);
	}

	@Override
	public String type()
	{
		return "meuble";
	}
}
