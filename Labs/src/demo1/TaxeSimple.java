package demo1;

public class TaxeSimple extends Bien {

	public TaxeSimple(float prix) {
		super(prix);
		//	M�thode 1
		ajustementTaxe = 1.05F;
	}

	@Override
	public float prix()
	{
		//	M�thode 1
		return prixEtalage * ajustementTaxe;
		//	M�thode 2
		//return prixEtalage * 1.05F;
	}
}
