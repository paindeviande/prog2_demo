package demo1;

public class TaxeDouble extends Bien {

	public TaxeDouble(float prix) {
		super(prix);
		//	M�thode 1
		ajustementTaxe = 1.14975F;
	}
	
	@Override
	public float prix()
	{
		//	M�thode 1
		return prixEtalage * ajustementTaxe;
		//	M�thode 2
		//return prixEtalage * 1.14975F;
	}

}
