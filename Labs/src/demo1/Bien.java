package demo1;

public class Bien {
	
	protected float prixEtalage;
	//	M�thode 1
	protected float ajustementTaxe;

	public Bien(float prix)
	{
		prixEtalage = prix;
		ajustementTaxe = 1;
	}
	
	public float prix()
	{
		//	M�thode 1
		return prixEtalage * ajustementTaxe;
		
		//	M�thode 2
		//return prixEtalage;
	}
	
	public String type()
	{
		return "bien";
	}
}
