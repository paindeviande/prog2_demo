package demo1;

public class Couleur {

	private int rouge, vert, bleu;
	
	public Couleur(int r, int v, int b)
	{
		setRouge(r);
		setVert(v);
		setBleu(b);
	}
	
	public void blanchir()
	{
		setRouge(( rouge + 255 ) / 2);
		setVert(( vert + 255 ) / 2);
		setBleu(( bleu + 255 ) / 2);
	}

	@Override
	public String toString() {
		return "Couleur [rouge=" + rouge + ", vert=" + vert + ", bleu=" + bleu + "]";
	}

	public int getRouge() {
		return rouge;
	}

	public void setRouge(int value) {
		if(value >= 0 && value <= 255)
			this.rouge = value;
		else
			System.out.println("Valeur invalide :( \n");
	}

	public int getVert() {
		return vert;
	}

	public void setVert(int value) {
		if(value >= 0 && value <= 255)
			this.vert = value;
		else
			System.out.println("Valeur invalide :( \n");
	}

	public int getBleu() {
		return bleu;
	}

	public void setBleu(int value) {
		if(value >= 0 && value <= 255)
			this.bleu = value;
		else
			System.out.println("Valeur invalide :( \n");
	}
	
	
}
