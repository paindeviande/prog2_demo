package demo1;

public class Legume extends NonTaxable {

	public Legume(float prix) {
		super(prix);
	}

	@Override
	public String type()
	{
		return "legume";
	}
}
