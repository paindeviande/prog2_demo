package demo1;

import java.util.ArrayList;

public class Principal {

	public static void main(String[] args) 
	{
		 Couleur c1 = new Couleur( 1, 4, 6 );
		 Couleur c2 = c1;
		 System.out.println( c2.toString() );
		 
		 System.out.println("\n SetRouge 100 \n");
		 c2.setRouge( 100 );
		 System.out.println( c1.toString() );
		 System.out.println( c2.toString() );
		 
		 System.out.println("\n BLANCHIR \n");
		 c1.blanchir();
		 System.out.println( c1.toString() );
		 System.out.println( c2.toString() );
	}
	
	/*public static void main(String[] args)
	{
		int i;
		Bien tab[] = new Bien[10];
		//ArrayList<Bien> array = new ArrayList<>();
		
		for(i = 0; i<10; i++)
		{
			if(i%3 == 0)
				tab[i] = new Legume(3 * i);
				//array.add(new Legume(3 * i));
			else if(i%3 == 1)
				tab[i] = new Livre(10 * i);
				//array.add(new Livre(10 * i));
			else
				tab[i] = new Meuble(75 * i);
				//array.add(new Meuble(75 * i));
			System.out.println("Je suis le " + tab[i].type() + " avec le ID " + (i+1) + " et je co�te: " + tab[i].prix() + " $");
		}
		
		System.out.println("Co�t total: " + calculFacture(tab) + " $");
	}*/
	
	//public static float calculFacture(ArrayList<Bien> tabBiens)
	public static float calculFacture(Bien tabBiens[])
	{
		float valeur = 0;
		int i;
		
		//	Lambda expression (vu plus tard dans la session)
		//for(Bien bien: tabBiens)
		//	valeur += bien.prix();
		
		for(i = 0; i< tabBiens.length; i++)
			valeur += tabBiens[i].prix();
		//for(i = 0; i < tabBiens.size(); i++)
		//	valeur += tabBiens.get(i).prix();
		return valeur;
	}

}
